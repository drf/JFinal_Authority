package com.jayqqaa12.system.controller;

import com.jayqqaa12.UrlConfig;
import com.jayqqaa12.jbase.jfinal.ext.Controller;
import com.jayqqaa12.jbase.util.L;
import com.jayqqaa12.shiro.ShiroCache;
import com.jayqqaa12.shiro.ShiroInterceptor;
import com.jayqqaa12.system.model.Res;
import com.jayqqaa12.system.validator.ResValidator;
import com.jfinal.aop.Before;
import com.jfinal.ext.route.ControllerBind;

@ControllerBind(controllerKey = "/system/res", viewPath = UrlConfig.SYSTEM)
public class ResController extends Controller<Res>
{

	public void tree()
	{
		Integer pid = getParaToInt("id");
		int type = getParaToInt("type", Res.TYPE_MEUE);
		renderJson(Res.dao.getTree(pid, type));

	}

	public void list()
	{
		renderJson(Res.dao.listOrderBySeq());
	}

	public void delete()
	{
		renderJsonResult(Res.dao.deleteByIdAndPid(getParaToInt("id")));

		removeAuthorization();
	}

	@Before(value = { ResValidator.class })
	public void add()
	{
		renderJsonResult(getModel(Res.class).save());
		removeAuthorization();
	}

	@Before(value = { ResValidator.class })
	public void edit()
	{
		Res res = getModel(Res.class);

		if (Res.dao.pidIsChild(res.getId(), res.getPid())) renderJsonError("父节点不能为子节点");
		else
		{
			renderJsonResult(res.update());
			removeAuthorization();
		}

	}

	private void removeAuthorization()
	{
		ShiroCache.clearAuthorizationInfoAll();
		ShiroInterceptor.updateUrls();
	}

}
